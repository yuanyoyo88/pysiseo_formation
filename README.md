---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.3.3
  kernelspec:
    display_name: Python [conda env:root] *
    language: python
    name: conda-root-py
---

# Formation Python, calcul scientifique et analyse de donnée

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/celliern%2Fpysiseo_formation/master?filepath=README.ipynb)

## Partie 1 (7 h) - les fondamentaux

- [Introduction au langage Python](notebooks/01-fondamentaux/fonda_00-introduction.ipynb)
  - Petite perspective
  - Installation de via [anaconda](https://www.anaconda.com/)
  - Premier contact : utilisation des notebooks [jupyter](https://jupyter.org/)
  - Aparté sur les conventions de style ([PEP8](https://www.python.org/dev/peps/pep-0008/))
- Apprentissage par l'exemple : lecture et analyse de fichiers de donnée
  - [Utilisations des variables](notebooks/01-fondamentaux/fonda_01-variables.ipynb)
  - [Analyser des données tabulaires](notebooks/01-fondamentaux/fonda_02-donnees_tabulaires.ipynb)
  - [Affichage des données](notebooks/01-fondamentaux/fonda_03-simple_vizu.ipynb)
  - [Répétition d'action avec les boucles](notebooks/01-fondamentaux/fonda_04-boucles.ipynb)
  - [Stockage de valeurs dans les listes](notebooks/01-fondamentaux/fonda_05-listes_tuples.ipynb)
  - [Application au traitement de nombreux fichiers](notebooks/01-fondamentaux/fonda_06-app_multiples_fichiers.ipynb)
  - [Programmation conditionnelle : des actions différentes pour différentes valeurs](notebooks/01-fondamentaux/fonda_07-conditions.ipynb)
  - [Structurer le code avec les fonctions](notebooks/01-fondamentaux/fonda_08-fonctions.ipynb)
  - [Rendre son code plus robuste, gestion des erreurs et debug](notebooks/01-fondamentaux/fonda_09-robustesse_erreur_debug.ipynb)
- Bonus: les notebooks sont vides ou presques, mais les thématiques pourront être abordées à la demande
  - [Créer un package Python](notebooks/01-fondamentaux/fonda_10-packaging.ipynb)
  - [La programmation orientée objet, côté développeur](notebooks/01-fondamentaux/fonda_20-POO.ipynb)
  - [Méthodes magiques et Duck Typing](notebooks/01-fondamentaux/fonda_21-dunder_method_duck_typing.ipynb)
  - [Décorateurs](notebooks/01-fondamentaux/fonda_30-decorateurs.ipynb)
  - [Générateurs](notebooks/01-fondamentaux/fonda_31-generateur.ipynb)
  - [Context Managers](notebooks/01-fondamentaux/fonda_32-context_manageur.ipynb)

## Partie 2 (7h) - outils d'analyse de donnée

- [Mise en contexte : le stack scientifique python](notebooks/02-analyse_num/num_00-contexte.ipynb)
- Introduction au calcul scientifique en python
  - [La brique de base : `numpy`](notebooks/02-analyse_num/num_01-scistack_numpy.ipynb)
  - [La boîte à outil : `scipy`](notebooks/02-analyse_num/num_02-scistack_scipy.ipynb)
  - [Statistique et données tabulaires : `pandas`](notebooks/02-analyse_num/num_03-scistack_stats_pandas.ipynb)
  - [La visualisation : `matplotlib`, `pyviz`](notebooks/02-analyse_num/num_04-scistack_visualisation.ipynb)
- [`pandas` pour l'analyse de séries temporelles](notebooks/02-analyse_num/num_06-pandas_intro.ipynb)
  - [Lecture et écriture de set de donnée](notebooks/02-analyse_num/num_07-pandas_io.ipynb)
  - [Sélection des données : indexation simple et avancé](notebooks/02-analyse_num/num_08-pandas_indexation.ipynb)
  - [Renforcement des données : gestion des données manquantes](notebooks/02-analyse_num/num_09-pandas_donnees_manquantes.ipynb)
  - [Gestion des données temporelles, rééchantillonage](notebooks/02-analyse_num/num_10-pandas_serie_temporelles.ipynb)
- Bonus (si le temps): les thématiques pourront être abordées à la demande
  - [Optimisation](notebooks/02-analyse_num/num_20-optimisation.ipynb)
  - [Analyse de sensibilité](notebooks/02-analyse_num/num_21-sensitivity_analysis.ipynb)
  - [Introduction au Machine Learning](notebooks/02-analyse_num/num_22-intro_ML.ipynb)
  - [Parallélisation](notebooks/02-analyse_num/num_30-parallelisation.ipynb)

```python

```
